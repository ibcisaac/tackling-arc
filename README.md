# Tackling ARC (Màster Universitari en Ciència de Dades (UOC) &ndash; Master's thesis)

Source code corresponding to the Master's thesis: **What does Artificial General Intelligence mean?
An inquiry to the algorithms that ‘think’**.

## Summary

The [Abstraction and Reasoning Corpus (ARC)](https://github.com/fchollet/ARC) is a benchmark proposed by François Chollet in 2019, with the intention of serving as an AI benchmark for assessing the capacity of generalization and flexibility of thinking and learning similar to that of humans.

We use classification Machine Learning methods in order to tackle the benchmark, as well as a simple approach with Convolutional Neural Networks.


## Source code

### utils
Python code with useful functions, mainly related to ARC image plotting and data reading.

### baseline_features
Jupyter notebook for creating the baseline features for all tasks.

### clustering_features
Jupyter notebook for creating the clustering features for all tasks.

### results_report
Jupyter notebook containing the training and testing procedures for the models, as well as the generation of the charts included in the thesis.

### CNN
Jupyter notebook containing the simple testing of a CNN model.


## Folders

### features

Folder containing pickle files (dictionaries) of the preprocessed features for each task.