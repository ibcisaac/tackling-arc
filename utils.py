import pandas as pd
import numpy as np
from PIL import ImageColor
import json
import os
import pickle

import matplotlib.pyplot as plt
from matplotlib import colors


# ===============================
#        READ ARC TASKS
# ===============================

color_ints = list(range(10)) + [-1]

int2colorname = {
    0: 'black',
    1: 'blue',
    2: 'red',
    3: 'green',
    4: 'yellow',
    5: 'gray',
    6: 'pink',
    7: 'orange',
    8: 'l_blue',
    9: 'brown',
    -1: 'nothing',
}

def parse_tasks_as_np(task_dicts):
    # convert input and output "images" (lists for color numbers) to numpy arrays
    for task_id, task in task_dicts.items():
        for task_train in task['train']:
            task_train['input'] = np.array(task_train['input'])
            task_train['output'] = np.array(task_train['output'])
        for task_test in task['train']:
            task_test['input'] = np.array(task_test['input'])
            task_test['output'] = np.array(task_test['output'])
    return task_dicts

def read_tasks(data_path, task_ids=[]):
    # read ARC tasks as dictionaries
    train = {}
    test = {}
    evaluation = {}

    for folder in ['training', 'evaluation', 'test']:
        for fname in os.listdir(os.path.join(data_path, folder)):
            fpath = os.path.join(data_path, folder, fname)
            with open(fpath, 'r') as f:
                task_id = fname.split('.')[0]
                if len(task_ids) == 0 or task_id in task_ids:
                    if folder == 'training':
                        train[task_id] = json.load(f)
                    elif folder == 'evaluation':
                        evaluation[task_id] = json.load(f)
                    else:
                        test[task_id] = json.load(f)
    
    # parse "image" lists as numpy arrays
    train = parse_tasks_as_np(train)
    evaluation = parse_tasks_as_np(evaluation)
    test = parse_tasks_as_np(test)

    return train, evaluation, test



# ===============================
#        READ ARC FEATURES
# ===============================

def read_task_features(tasks_path):
    with open(tasks_path, 'rb') as f:
        return pickle.load(f)



# ===============================
#        PLOT FUNCTIONS
# ===============================

colors_list = ['#000000', '#0074D9','#FF4136','#2ECC40','#FFDC00',
               '#AAAAAA', '#F012BE', '#FF851B', '#7FDBFF', '#870C25']

# with extra color for CNN
color_extra = '#111111'
color_extra_rgb = ImageColor.getcolor(color_extra, 'RGB')

color_map = colors.ListedColormap(colors_list)


def plot_image(obj, save_path=''):
    obj = np.array(obj)
    plt.tick_params(left = False, bottom = False)
    plt.yticks([x-0.5 for x in range(1+len(obj))], [])
    plt.xticks([x-0.5 for x in range(1+len(obj[0]))], [])
    norm = colors.Normalize(vmin=0, vmax=9)
    plt.grid(True,which='both', color='lightgrey', linewidth=0.5)
    plt.imshow(obj, cmap=color_map, norm=norm)
    
    if save_path:
        plt.savefig(save_path, dpi=300, transparent=True)
    plt.show()


def plot_objects(objects, titles=None):
    if titles is None:
        titles = np.full(len(objects), '')
        
    norm = colors.Normalize(vmin=0, vmax=9)
    fig, axs = plt.subplots(1, len(objects), figsize=(30,3),
                            gridspec_kw={'wspace':0.02, 'hspace':0.02}, squeeze=False)

    for i, (obj, title) in enumerate(zip(objects, titles)):
        obj = np.array(obj)
        axs[0,i].grid(True, which='both', color='lightgrey', linewidth=0.5)
#         axs[i].axis('off')
        shape = ' '.join(map(str, obj.shape))
        axs[0,i].set_title(f"{title} {shape}")
        axs[0,i].set_yticks([x-0.5 for x in range(1+len(obj))])
        axs[0,i].set_xticks([x-0.5 for x in range(1+len(obj[0]))])
        axs[0,i].set_yticklabels([])     
        axs[0,i].set_xticklabels([])
        axs[0,i].imshow(obj, cmap=color_map, norm=norm)
    plt.show()
    
def plot_task(task):
    objects = []
    titles = []
    for key in ['train', 'test']:
        for obj in task[key]:
            objects.append(obj['input'])
            titles.append(f'{key} IN')
            if 'output' in obj:
                objects.append(obj['output'])
                titles.append(f'{key} OUT')
    plot_objects(objects, titles)




# ===============================
#        IMAGE FUNCTIONS
# ===============================


def get_colors_freq(image):
    # get colors frequency from an image
    return pd.Series(np.array(image).flatten()).value_counts().to_dict()

def are_same_shape(img1, img2):
    img1 = np.array(img1)
    img2 = np.array(img2)
    return img1.shape == img2.shape

def get_images_diff(img1, img2):
    if not are_same_shape(img1, img2):
        return np.inf
    # flatten just in case
    img1_flat = np.array(img1).flatten()
    img2_flat = np.array(img2).flatten()
    return sum(img1_flat != img2_flat)